<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Peternakan - @yield('title')</title>
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
</head>
<body>
    
    <div id="app">
        @include('flash')

        @yield('content')
    </div>

    <script src="{{ URL::asset('js/app.js') }}" type="text/javascript" async defer></script>
    @stack('scripts')
</body>
</html>