@extends('master._layout')

@section('title', 'Super Admin')

@section('content')

<div class="container mt-2">
    <div class="card-box">
        <ul class="ul-nav">
            <li><a href="/adminUtama" class="btn btn-info">Home</a></li>
            <li>
                <a class="btn btn-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>

<div class="container pt-2">
    <div class="card-box">
        <h3>Selamat datang di Halaman Super Admin..</h3>
    </div>
</div>

<div class="container pt-4">
    <div class="m-2 p-2">
        <a href="/adminUtama/create" class="btn btn-success"> <i class="fa fa-plus"></i> Tambah User</a>

    </div>
    
    <div class="card-box">
        <table class="table table-hover table-bordered table-responsive-lg">
             <?php $i=1; ?>
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Username</th>
                    <th scope="col">Name</th>
                    <th scope="col">Group</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Kota</th>
                    <th scope="col">Telepon</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $user->username }} </td>
                    <td> {{ $user->name }} </td>
                    <td> {{ $user->group }} </td>
                    <td> {{ $user->alamat }} </td>
                    <td> {{ $user->kota }} </td>
                    <td> {{ $user->telepon }} </td>
                    <td> {{ $user->email }} </td>
                    <td>
                        <a class="btn btn-warning" href="{{ route('adminUtama.edit',$user->id)}}"><i class="fa fa-cog"></i> Edit</a> <br>
                        <div class="mt-2"></div>
                        {{-- <a class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a> --}}
                        <form action="{{ route('adminUtama.destroy', $user->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i> Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
