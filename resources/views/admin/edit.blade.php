@extends('master._layout')

@section('title', 'Edit User')

@section('content')
<div class="container p-5">
    <div class="card-box">
        <h5>Edit User</h5>

        <form method="POST" action="{{ route('adminUtama.update', $user->id) }}">
            @method('PATCH')
            @csrf

            <div class="form-group row">
                <label for="username" class="col-md-4 col-form-label text-md-left">{{ __('Username') }}</label>

                <div class="col-md-6">
                    <input id="username" type="text"
                        class="form-control" name="username"
                        value="{{ $user->username }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control"
                        name="name" value="{{ $user->name }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control"
                        name="email" value="{{ $user->email }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-left">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password">
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm"
                    class="col-md-4 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                </div>
            </div>

            <div class="form-group row">
                <label for="group" class="col-md-4 col-form-label text-md-left">{{ __('ID Group') }}</label>

                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">Group</label>
                        </div>
                        <select class="custom-select" id="inputGroupSelect01" name="group">
                            <option value="{{ $user->group }}">{{ $user->group }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="alamat" class="col-md-4 col-form-label text-md-left">{{ __('Alamat') }}</label>

                <div class="col-md-6">
                    <input id="alamat" type="text" class="form-control"
                        name="alamat" value="{{ $user->alamat }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="kota" class="col-md-4 col-form-label text-md-left">{{ __('Kota') }}</label>

                <div class="col-md-6">
                    <input id="kota" type="text" class="form-control"
                        name="kota" value="{{ $user->kota }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="telepon" class="col-md-4 col-form-label text-md-left">{{ __('Telepon') }}</label>

                <div class="col-md-6">
                    <input id="telepon" type="text"
                        class="form-control" name="telepon"
                        value="{{ $user->telepon }}">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-info">
                        {{ __('Edit') }}
                    </button>
                </div>
            </div>

            @include('error')
        </form>
    </div>
</div>
@endsection
