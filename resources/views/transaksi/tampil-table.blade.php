@extends('master._layout') @section('title', 'Transaksi Penjual dan Peternak')

<div class="container">
    <div class="card-box">
        <div class="d-flex justify-content-between align-items-center">
            <div class="box">
                <a href="/pembeli" class="btn btn-info">Home</a>
            </div>
            <div class="sidebar__logout">
                <a class="btn btn-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>

@section('content')
<div class="container pt-4">
    @foreach ($pembeli as $pembeli)
        <div class="card-box">
            <h3>Selamat datang, <span class="badge badge-dark">{{ $pembeli->user->username }}</span></h3>
            <p>Berikut adalah History Transaksi anda</p>
        </div>
    @endforeach
</div>

<div class="container">
    <div class="card-box">
        <table class="table table-hover table-bordered table-responsive-lg">
             <?php $i=1; ?>
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Peternak</th>
                    <th scope="col">Pembeli</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transaksis as $transaksi)
                <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $transaksi->peternak_id }} </td>
                    <td> {{ $transaksi->pembeli_id }} </td>
                    <td> {{ $transaksi->nama }} </td>
                    <td> {{ $transaksi->alamat }} </td>
                    <td> {{ $transaksi->jumlah }} </td>
                    <td>
                        <a class="btn btn-warning" href="#"><i class="fa fa-cog"></i> Edit</a> <br>
                        <div class="mt-2"></div>
                        <form action="#" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i> Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection