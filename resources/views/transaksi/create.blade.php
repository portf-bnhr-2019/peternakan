@extends('master._layout')

@section('title', 'Buat Order')

<div class="container">
    <div class="card-box">
        <div class="d-flex justify-content-between align-items-center">
            <div class="box">
                <a href="/pembeli" class="btn btn-info">Home</a>
            </div>
            <div class="sidebar__logout">
                <a class="btn btn-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>

@section('content')
<div class="container p-5">
    <div class="card-box">
        <h5>Buat Order Baru</h5>

        <form method="POST" action="{{ route('transaksi.store') }}">
            @csrf

            <div class="form-group row">
                <label for="peternak"
                    class="col-md-4 col-form-label text-md-left">{{ __('Nama Peternak') }}</label>

                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <p>: {{ $kandangs->username }}</p>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="group"
                    class="col-md-4 col-form-label text-md-left">{{ __('Nama Pembeli') }}</label>

                <div class="col-md-6">
                    <div class="input-group mb-3">
                        @foreach ($pembelis as $pembeli)
                            <p>: {{ $pembeli->username }}</p>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="nama" class="col-md-4 col-form-label text-md-left">{{ __('Nama') }}</label>

                <div class="col-md-6">
                    <input id="nama" type="text" class="form-control"
                        name="nama" value="{{ old('nama') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="alamat" class="col-md-4 col-form-label text-md-left">{{ __('Alamat Penerima') }}</label>

                <div class="col-md-6">
                    <input id="alamat" type="text" class="form-control"
                        name="alamat" value="{{ old('alamat') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="jumlah" class="col-md-4 col-form-label text-md-left">{{ __('Jumlah') }}</label>

                <div class="col-md-6">
                    <input id="jumlah" type="text" class="form-control"
                        name="jumlah" value="{{ old('stok') }}" required>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Order') }}
                    </button>
                </div>
            </div>

            @include('error')
        </form>
    </div>
</div>
@endsection
