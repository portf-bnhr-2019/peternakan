@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="username"
                                class="col-md-4 col-form-label text-md-left">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="username" type="text"
                                    class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
                                    name="username" value="{{ old('username') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                    value="{{ old('name') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                    value="{{ old('email') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="group"
                                class="col-md-4 col-form-label text-md-left">{{ __('ID Group') }}</label>

                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Group</label>
                                    </div>
                                    <select class="custom-select" id="inputGroupSelect01" name="group">
                                        <option>Pilih...</option>
                                        <option value="2" @if(old('group') == '2')selected @endif>Penjual</option>
                                        <option value="3" @if(old('group') == '3')selected @endif>Peternak</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-left">{{ __('Alamat') }}</label>

                            <div class="col-md-6">
                                <input id="alamat" type="text"
                                    class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" name="alamat"
                                    value="{{ old('alamat') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="kota" class="col-md-4 col-form-label text-md-left">{{ __('Kota') }}</label>

                            <div class="col-md-6">
                                <input id="kota" type="text"
                                    class="form-control{{ $errors->has('kota') ? ' is-invalid' : '' }}" name="kota"
                                    value="{{ old('kota') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telepon"
                                class="col-md-4 col-form-label text-md-left">{{ __('Telepon') }}</label>

                            <div class="col-md-6">
                                <input id="telepon" type="text"
                                    class="form-control{{ $errors->has('telepon') ? ' is-invalid' : '' }}"
                                    name="telepon" value="{{ old('telepon') }}" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>

                        @include('error')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
