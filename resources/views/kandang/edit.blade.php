@extends('master._layout') @section('title', 'Edit Kondisi Kandang') @section('content')
<div class="container p-5">
    <div class="card-box">
        <h2 class="">Edit Kondisi Kandang</h2>

        <form method="POST" action="{{ route('kandang.update', $kandang->id) }}" enctype="multipart/form-data">
            @method('PATCH') @csrf

            <div class="form-group row">
                <label for="suhu_1" class="col-md-4 col-form-label text-md-left">{{ __('Suhu 1') }}</label>

                <div class="col-md-6">
                    <input id="suhu_1" type="text" class="form-control" name="suhu_1" value="{{ old('suhu_1', $kandang->suhu_1) }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="suhu_2" class="col-md-4 col-form-label text-md-left">{{ __('Suhu 2') }}</label>

                <div class="col-md-6">
                    <input id="suhu_2" type="text" class="form-control" name="suhu_2" value="{{ old('suhu_2', $kandang->suhu_2) }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="suhu_3" class="col-md-4 col-form-label text-md-left">{{ __('Suhu 3') }}</label>

                <div class="col-md-6">
                    <input id="suhu_3" type="text" class="form-control" name="suhu_3" value="{{ old('suhu_1', $kandang->suhu_1) }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="kelembapan_1" class="col-md-4 col-form-label text-md-left">{{ __('Kelembapan 1') }}</label>

                <div class="col-md-6">
                    <input id="kelembapan_1" type="text" class="form-control" name="kelembapan_1" value="{{ old('kelembapan_1', $kandang->kelembapan_1) }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="kelembapan_2" class="col-md-4 col-form-label text-md-left">{{ __('Kelembapan 1') }}</label>

                <div class="col-md-6">
                    <input id="kelembapan_2" type="text" class="form-control" name="kelembapan_2" value="{{old('kelembapan_2', $kandang->kelembapan_2)}}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="kelembapan_3" class="col-md-4 col-form-label text-md-left">{{ __('Kelembapan 1') }}</label>

                <div class="col-md-6">
                    <input id="kelembapan_3" type="text" class="form-control" name="kelembapan_3" value="{{old('kelembapan_3', $kandang->kelembapan_3)}}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="stok" class="col-md-4 col-form-label text-md-left">{{ __('Stok') }}</label>

                <div class="col-md-6">
                    <input id="stok" type="text" class="form-control" name="stok" value="{{old('stok', $kandang->stok)}}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="harga" class="col-md-4 col-form-label text-md-left">{{ __('Harga') }}</label>

                <div class="col-md-6">
                    <input id="harga" type="text" class="form-control" name="harga" value="{{old('harga', $kandang->harga)}}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="gambar" class="col-md-4 col-form-label text-md-left">{{ __('Gambar') }}</label>

                <div class="col-md-6">
                    <input type="file" class="form-control-file" id="gambar" name="gambar" value="{{old('gambar', $kandang->gambar)}}">
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <img src="{{ URL::asset('image/ayam/'.$kandang->gambar)}}" alt="Ayam" style="max-width: 200px;">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Edit') }}
                    </button>
                </div>
            </div>

            @include('error')
        </form>
    </div>
</div>
@endsection