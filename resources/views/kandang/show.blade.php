@extends('master._layout')

@section('title', 'Lihat Peternak')

@section('content')

<div class="container mt-2">
    <div class="card-box">
        <ul class="ul-nav">
            <li><a href="/kandang" class="btn btn-info">Home</a></li>
            <li>
                <a class="btn btn-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>

<div class="container">
    <div class="card-box">
        <div class="title-kandang">
            <h4 class="text-center">Kondisi Kandang Anda</h4>
            <a href="{{ route('kandang.edit',$kandang->id)}}" class="btn btn-secondary"><i class="fa fa-cog"> Edit
                    Kondisi Kandang</i></a>
        </div>
        <div class="row d-flex align-items-lg-center">
            <div class="col-md-12">
                <div class="card-box--kandang__img text-center mb-2">
                    @if($kandang->gambar)
                    <img src="{{ URL::asset('image/ayam/'.$kandang->gambar)}}" alt="Ayam" style="max-width: 200px;">
                    @else
                    <img src="{{ URL::asset('image/ayamku.jpg') }}" class="rounded" />
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Suhu 1
                            </div>
                            <div class="card-body">
                                <h3 class="card-title"> <i class="fas fa-thermometer-half"></i>
                                    {{ $kandang->suhu_1}}&deg; </h3>
                                <p class="card-text">Suhu kandang anda {{ $kandang->suhu_1}}&deg; </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Kelembapan 1
                            </div>
                            <div class="card-body">
                                <h3 class="card-title"> <i class="fas fa-tint"></i> {{ $kandang->kelembapan_1}}% </h3>
                                <p class="card-text">Kelembapan kandang anda {{ $kandang->kelembapan_1}}% </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Suhu 2
                            </div>
                            <div class="card-body">
                                <h3 class="card-title"> <i class="fas fa-thermometer-half"></i>
                                    {{ $kandang->suhu_2}}&deg; </h3>
                                <p class="card-text">Suhu kandang anda {{ $kandang->suhu_2}}&deg; </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Kelembapan 2
                            </div>
                            <div class="card-body">
                                <h3 class="card-title"> <i class="fas fa-tint"></i> {{ $kandang->kelembapan_2}}% </h3>
                                <p class="card-text">Kelembapan kandang anda {{ $kandang->kelembapan_2}}% </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Suhu 3
                            </div>
                            <div class="card-body">
                                <h3 class="card-title"> <i class="fas fa-thermometer-half"></i>
                                    {{ $kandang->suhu_3}}&deg; </h3>
                                <p class="card-text">Suhu kandang anda {{ $kandang->suhu_3}}&deg; </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Kelembapan 3
                            </div>
                            <div class="card-body">
                                <h3 class="card-title"> <i class="fas fa-tint"></i> {{ $kandang->kelembapan_3}}% </h3>
                                <p class="card-text">Kelembapan kandang anda {{ $kandang->kelembapan_3}}% </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card text-center">
                    <div class="card-header">
                        Stok Anda
                    </div>
                    <div class="card-body">
                        <h3 class="card-title"> <i class="fas fa-boxes"></i> {{ $kandang->stok}} item</h3>
                        <p class="card-text">Stok anda {{ $kandang->stok}} item</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card text-center">
                    <div class="card-header">
                        Harga
                    </div>
                    <div class="card-body">
                        <h3 class="card-title"> <i class="fas fa-money-bill-wave"></i>
                            {{ number_format($kandang->harga, 2, ',', '.') }} Rupiah</h3>
                        <p class="card-text">Harga {{ number_format($kandang->harga, 2, ',', '.') }} Rupiah</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
