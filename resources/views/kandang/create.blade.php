@extends('master._layout')

@section('title', 'Buat Kondisi Kandang')

@section('content')
<div class="container p-5">
    <div class="card-box">
        <h5>Buat Kondisi Kandang</h5>

        <form method="POST" action="{{ route('kandang.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <label for="tanggal" class="col-md-4 col-form-label text-md-left">{{ __('Tanggal') }}</label>

                <div class="col-md-6">
                    <input id="tanggal" type="date"
                        class="form-control{{ $errors->has('tanggal') ? ' is-invalid' : '' }}" name="tanggal"
                        value="{{ old('tanggal') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="waktu" class="col-md-4 col-form-label text-md-left">{{ __('Waktu') }}</label>

                <div class="col-md-6">
                    <input id="waktu" type="time" class="form-control{{ $errors->has('waktu') ? ' is-invalid' : '' }}"
                        name="waktu" value="{{ old('waktu') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="suhu_1" class="col-md-4 col-form-label text-md-left">{{ __('Suhu 1') }}</label>

                <div class="col-md-6">
                    <input id="suhu_1" type="text" class="form-control{{ $errors->has('suhu_1') ? ' is-invalid' : '' }}"
                        name="suhu_1" value="{{ old('suhu_1') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="suhu_2" class="col-md-4 col-form-label text-md-left">{{ __('Suhu 2') }}</label>

                <div class="col-md-6">
                    <input id="suhu_2" type="text" class="form-control{{ $errors->has('suhu_2') ? ' is-invalid' : '' }}"
                        name="suhu_2" value="{{ old('suhu_2') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="suhu_3" class="col-md-4 col-form-label text-md-left">{{ __('Suhu 3') }}</label>

                <div class="col-md-6">
                    <input id="suhu_3" type="text" class="form-control{{ $errors->has('suhu_3') ? ' is-invalid' : '' }}"
                        name="suhu_3" value="{{ old('suhu_3') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="kelembapan_1" class="col-md-4 col-form-label text-md-left">{{ __('Kelembapan 1') }}</label>

                <div class="col-md-6">
                    <input id="kelembapan_1" type="text"
                        class="form-control{{ $errors->has('kelembapan_1') ? ' is-invalid' : '' }}" name="kelembapan_1"
                        value="{{ old('kelembapan_1') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="kelembapan_2" class="col-md-4 col-form-label text-md-left">{{ __('Kelembapan 1') }}</label>

                <div class="col-md-6">
                    <input id="kelembapan_2" type="text"
                        class="form-control{{ $errors->has('kelembapan_2') ? ' is-invalid' : '' }}" name="kelembapan_2"
                        value="{{ old('kelembapan_2') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="kelembapan_3" class="col-md-4 col-form-label text-md-left">{{ __('Kelembapan 1') }}</label>

                <div class="col-md-6">
                    <input id="kelembapan_3" type="text"
                        class="form-control{{ $errors->has('kelembapan_3') ? ' is-invalid' : '' }}" name="kelembapan_3"
                        value="{{ old('kelembapan_3') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="telepon" class="col-md-4 col-form-label text-md-left">{{ __('Telepon') }}</label>

                <div class="col-md-6">
                    <input id="telepon" type="text"
                        class="form-control{{ $errors->has('telepon') ? ' is-invalid' : '' }}" name="telepon"
                        value="{{ old('telepon') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="stok" class="col-md-4 col-form-label text-md-left">{{ __('Stok') }}</label>

                <div class="col-md-6">
                    <input id="stok" type="text" class="form-control{{ $errors->has('stok') ? ' is-invalid' : '' }}"
                        name="stok" value="{{ old('stok') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="harga" class="col-md-4 col-form-label text-md-left">{{ __('Harga') }}</label>

                <div class="col-md-6">
                    <input id="harga" type="text" class="form-control{{ $errors->has('harga') ? ' is-invalid' : '' }}"
                        name="harga" value="{{ old('harga') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="gambar" class="col-md-4 col-form-label text-md-left">{{ __('Gambar') }}</label>

                <div class="col-md-6">
                    <input type="file" class="form-control-file{{ $errors->has('gambar') ? ' is-invalid' : '' }}" id="gambar"
                    name="gambar" value="{{ old('gambar') }}" required>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>

            @include('error')
        </form>
    </div>
</div>
@endsection
