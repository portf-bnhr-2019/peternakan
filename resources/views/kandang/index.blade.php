@extends('master._layout')

@section('title', 'Peternak')

@section('nm')
Kandang
@endsection

@section('content')

<div class="container mt-2">
    <div class="card-box">
        <ul class="ul-nav">
            <li><a href="/kandang" class="btn btn-info">Home</a></li>
            <li>
                <a class="btn btn-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>

<div class="container pt-2">
    @foreach ($kandangs as $kandang)
    <div class="card-box">
        <h3>Selamat datang {{ $kandang->user->username }}</h3>
    </div>
    @endforeach
</div>

<div class="container">
    <div class="mt-3 mb-3">
    </div>
    <div class="card-box--kandang">
        @foreach ($kandangs as $kandang)
        <h5>Kondisi Kandang Anda </h5>
        <span class="card-box__tanggal">Tanggal : {{ \Carbon\Carbon::parse($kandang->tanggal)->format('d-m-Y')}}</span>
        <span class="card-box__tanggal">Waktu : {{ \Carbon\Carbon::parse($kandang->waktu)->format('H:m:i')}}</span>
        <span class="card-box__tanggal">Stok : {{ $kandang->stok }}</span>
        <div class="card-box--kandang__img">
            @if($kandang->gambar)
                <img src="{{ URL::asset('image/ayam/'.$kandang->gambar)}}" alt="Ayam" style="max-width: 200px;">
            @else
                <img src="{{ URL::asset('image/ayamku.jpg') }}" class="rounded" />
            @endif
        </div>
        <a href="/kandang/{{ $kandang->id }}" class="btn btn-success"> <i class="fa fa-eye"></i> Show Detail</a>
        @endforeach
    </div>
</div>

@endsection
