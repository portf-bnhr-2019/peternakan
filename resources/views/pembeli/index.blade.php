@extends('master._layout') @section('title', 'Transaksi Penjual dan Peternak')

<div class="container">
    <div class="card-box">
        <div class="d-flex justify-content-between align-items-center">
            <div class="box">
                <a href="/kandang" class="btn btn-info">Home</a>
            </div>
            <div class="sidebar__logout">
                <a class="btn btn-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>

@section('content')
<div class="container pt-4">
    @foreach ($pembeli as $pembeli)
        <div class="card-box">
            <h3>Selamat datang, <span class="badge badge-dark">{{ $pembeli->user->username }}</span></h3>
            <p>Silahkan memilih produk dari para peternak terbaik kami ...</p>
        </div>
    @endforeach
</div>

<div class="container">
    <div class="row">
        @foreach ($kandang as $kandang)
            <div class="col-md-4">
                <div class="card-box--pembeli">
                    <div class="card-box--pembeli__title">
                        <p>Ayam {{ $kandang->user->username }}</p>
                    </div>
                    <div class="card-box--pembeli__price">
                        <span>Stok :</span> {{ $kandang->stok }}
                    </div>
                    <div class="card-box--pembeli__price">
                        <span>Harga :</span> Rp. {{ number_format($kandang->harga, 2, ',', '.') }}
                    </div>
                    <a href="#" class="card-box--pembeli__img">
                        @if($kandang->gambar)
                            <img src="{{ URL::asset('image/ayam/'.$kandang->gambar)}}" alt="Ayam">
                        @else
                            <img src="{{ URL::asset('image/ayamku.jpg') }}" class="rounded" />
                        @endif
                    </a>
                    <a href="/transaksi/{{ $kandang->id }}" class="btn btn-success"> <i class="fas fa-cart-plus"></i> Order</a>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection