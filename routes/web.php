<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/order', function () {
    return view('pembeli.order');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// GET EACH PAGE WITH RESOURCES
Route::resource('/adminUtama', 'AdminController');
Route::resource('/kandang', 'KandangController')->middleware('peternak');
Route::resource('/pembeli', 'PembeliController')->middleware('penjual');
Route::resource('/transaksi', 'TransaksiController');
Route::get('/tableTransaksi', 'TransaksiController@tampilTransaksiTabel');