<?php

namespace Peternakan;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $fillable = ['pembeli_id'];

    public function user()
    {
     	return $this->belongsTo(User::class, 'pembeli_id', 'id');
    }

    public function transaksis()
    {
        return $this->belongsTo('Peternakan\Transaksi', 'pembeli_id');
    }
}
 