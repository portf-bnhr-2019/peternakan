<?php

namespace Peternakan;

use Illuminate\Database\Eloquent\Model;

class Kandang extends Model
{
    protected $fillable = ['user_id', 'suhu_1', 'kelembapan_1', 'suhu_2', 'kelembapan_2', 'suhu_3', 'kelembapan_3', 'stok', 'harga', 'gambar'];
    
    public function user()
    {
     	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function transaksis()
    {
        return $this->hasMany('Peternakan\Transaksi', 'peternak_id', 'id');
    }

}
