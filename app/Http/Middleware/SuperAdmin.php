<?php

namespace Peternakan\Http\Middleware;

use Closure;
use Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->group == 1) {
            return $next($request);
        }
        elseif (Auth::check() && Auth::user()->group == 2) {
            return redirect('/pembeli')->with('error','Anda tidak memiliki akses sebagai admin');
        }
        else {
            return redirect('/kandang')->with('error','Anda tidak memiliki akses sebagai admin');
        }
    }
}
