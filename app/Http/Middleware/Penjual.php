<?php

namespace Peternakan\Http\Middleware;

use Closure;
use Auth;

class Penjual
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->group == 2 || Auth::user()->group == 1)) {
            return $next($request);
        }
        elseif (Auth::check() && Auth::user()->group == 3) {
            return redirect('/kandang')->with('error','Anda tidak memiliki akses sebagai penjual');
        }
        else {
            return redirect('/login');
        }
    }
}
