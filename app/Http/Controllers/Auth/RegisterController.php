<?php

namespace Peternakan\Http\Controllers\Auth;

use Peternakan\User;
use Auth;
use Peternakan\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Peternakan\Kandang;
use Peternakan\Pembeli;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected function redirectTo( ) {
        if (Auth::check() && Auth::user()->group == 1) {
            return('/adminUtama');
        }
        elseif (Auth::check() && Auth::user()->group == 2) {
            return('/pembeli');
        }
        else {
            return('/kandang');
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'min:6', 'max:255', 'unique:users'],
            'name' => ['required', 'string', 'min:6', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'group' => ['required'],
            'alamat' => ['required', 'string', 'min:6', 'max:255'],
            'kota' => ['required', 'string', 'min:6', 'max:255'],
            'telepon' => ['required', 'integer', 'min:10'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \Peternakan\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'group' => $data['group'],
            'alamat' => $data['alamat'],
            'kota' => $data['kota'],
            'telepon' => $data['telepon']
        ]);

        if ($user->group == 2) {
            $pembeli = new Pembeli;
            $pembeli->pembeli_id = $user->id;
            $pembeli->save();
        }elseif ($user->group == 3) {
            $kandang = new Kandang;
            $kandang->user_id = $user->id;
            $kandang->save();
        }

        return $user;

    }
}
