<?php

namespace Peternakan\Http\Controllers;

use Peternakan\Kandang;
use Illuminate\Http\Request;
use Auth;
use Peternakan\User;
use Illuminate\Support\Facades\File;

class KandangController extends Controller
{    
    public function index()
    {
        $user = Auth::user();
        $kandangs = $user->kandang;
        // dd($kandangs);
        return view('kandang.index', compact('kandangs'));
    }


    public function create()
    {
        return view('kandang.create');
    }


    public function store(Request $request)
    {        
        $this->validate($request, [
            'suhu_1' => 'required',
            'kelembapan_1' => 'required',
            'suhu_2' => 'required',
            'kelembapan_2' => 'required',
            'suhu_3' => 'required',
            'kelembapan_3' => 'required',
            'stok' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8192',
        ]);
            
        $kandang = new Kandang();
        $kandang->suhu_1            = $request->suhu_1;
        $kandang->kelembapan_1      = $request->kelembapan_1;
        $kandang->suhu_2            = $request->suhu_2;
        $kandang->kelembapan_2      = $request->kelembapan_2;
        $kandang->suhu_3            = $request->suhu_3;
        $kandang->kelembapan_3      = $request->kelembapan_3;
        $kandang->stok              = $request->stok;
        $kandang->gambar         = $request->gambar;
        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $filename = 'slide' . '-' . '.' . $gambar->getClientOriginalName();
            $location = 'image/ayam/';
            $request->file('gambar')->move($location, $filename);
            $kandang->gambar = $filename;
  
        }
        $kandang->save();
        return redirect('kandang')->with('success','Berhasil menambah Kondisi Kandang');
    }


    public function show(Kandang $kandang)
    {
        $users = Auth::user();
        return view('kandang.show', compact('kandang', 'users'));
    }


    public function edit($id)
    {
        $kandang = Kandang::findOrFail($id);
        return view('kandang.edit', compact('kandang'));
    }


    public function update(Request $request, $id)
    {
        $kandang = Kandang::find($id);
        $this->validate($request, [
            'suhu_1' => 'required',
            'kelembapan_1' => 'required',
            'suhu_2' => 'required',
            'kelembapan_2' => 'required',
            'suhu_3' => 'required',
            'kelembapan_3' => 'required',
            'stok' => 'required',
            'harga' => 'required',
            'gambar' => '',
        ]);

        $kandang->suhu_1            = $request->suhu_1;
        $kandang->kelembapan_1      = $request->kelembapan_1;
        $kandang->suhu_2            = $request->suhu_2;
        $kandang->kelembapan_2      = $request->kelembapan_2;
        $kandang->suhu_3            = $request->suhu_3;
        $kandang->kelembapan_3      = $request->kelembapan_3;
        $kandang->stok              = $request->stok;
        $kandang->harga              = $request->harga;
        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $filename = 'ayam' . '-' . $kandang->user->username . '.' . $gambar->getClientOriginalExtension();
            $location = public_path('image/ayam/');
            $request->file('gambar')->move($location, $filename);
            
            if($kandang->gambar)
            {
                $old_img = $kandang->gambar;
                $filepath = public_path().DIRECTORY_SEPARATOR.'image/ayam'.DIRECTORY_SEPARATOR.$kandang->gambar;
                try{
                    File::delete($filepath);
                }catch(FileNotFoundException $e)
                {
                    //file sudah dihapus
                }
            }
            $kandang->gambar = $filename;
        }
        $kandang->save();
        return redirect('kandang')->with('success','Berhasil mengedit Kondisi Kandang');
    }


    public function destroy(Kandang $kandang, $id)
    {
        $kandang = Kandang::findOrfail($id);

        if($kandang->gambar)
        {
            $old_img = $kandang->gambar;
            $filepath = public_path().DIRECTORY_SEPARATOR.'image/ayam'.DIRECTORY_SEPARATOR.$kandang->gambar;
            try{
                File::delete($filepath);
            }catch(FileNotFoundException $e){
                 //file sudah dihapus
            }
        }
        $kandang->delete();
    }
}
