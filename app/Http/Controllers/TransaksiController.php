<?php

namespace Peternakan\Http\Controllers;

use Peternakan\Transaksi;
use Illuminate\Http\Request;
use Peternakan\User;
use Peternakan\Kandang;
use Peternakan\Pembeli;
use Auth;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    public function index()
    {
        return view('transaksi.index');
    }

    public function create()
    {
        $user = Auth::user();
        $pembelis = $user->pembeli;
        return view('transaksi.create', compact('pembelis', 'kandangs'));
    }

    public function store(Request $request)
    {               
        $this->validate($request, [
            'peternak_id' => 'required',
            'pembeli_id' => 'required',
            'nama' => 'required|min:3',
            'alamat' => 'required|min:5',
            'jumlah' => 'required',
        ]);

        $transaksi = New Transaksi();
        $transaksi->peternak_id     = $request->peternak_id;
        $transaksi->pembeli_id      = $request->pembeli_id;
        $transaksi->nama            = $request->nama;
        $transaksi->alamat          = $request->alamat;
        $transaksi->jumlah          = $request->jumlah;
        $transaksi->save();

        $kandang = Kandang::where('id', '=', $request->peternak_id)->first();
        $stok = $kandang->stok;
        $hasil = $stok - $request->jumlah;
        $kandang = $kandang::find($request->peternak_id);
        $kandang->stok = $hasil;
        if ($stok == 0 || $hasil > $stok) {
            return redirect('pembeli')->with('error','Tidak Dapat Melakukan Order');
        }
        $kandang->update();

        return redirect('pembeli')->with('success','Berhasil Order');


    }


    public function show($id)
    {
        $user = Auth::user();
        $pembeli = $user->pembeli->first();
        $peternak = Kandang::find($id);
        return view('transaksi.show', compact('pembeli', 'peternak'));
    }

    public function edit(Transaksi $transaksi)
    {
        
    }

    public function update(Request $request, Transaksi $transaksi)
    {
        
    }

    public function destroy(Transaksi $transaksi)
    {
        
    }

    public function tampilTransaksiTabel()
    {
        $user = Auth::user();
        $pembeli = $user->pembeli;
        $transaksis = Transaksi::all();
        return view('transaksi.tampil-table', compact('pembeli', 'transaksis'));
    }
}
