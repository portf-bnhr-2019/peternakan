<?php

namespace Peternakan\Http\Controllers;

use Illuminate\Http\Request;
use Peternakan\User;
use Illuminate\Support\Facades\Hash;
use Peternakan\Kandang;
use Peternakan\Pembeli;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin');
    }
    
    public function index()
    {
        $users = User::all();
        return view('admin.index', compact('users'));
    }

    
    public function create()
    {
        return view('admin.create');
    }

    
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|min:5|max:12',
            'name' => 'required|min:5|max:12',
            'group' => 'required|string',
            'alamat' => 'required|min:5',
            'kota' => 'required',
            'telepon' => 'required|string',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = new User();
        $user->username = $request->username;
        $user->name = $request->name;
        $user->group = $request->group;
        $user->alamat = $request->alamat;
        $user->kota = $request->kota;
        $user->telepon = $request->telepon;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        
        if ($user->group == 2) {
            $pembeli = new Pembeli;
            $pembeli->pembeli_id = $user->id;
            $pembeli->save();
        }elseif ($user->group == 3) {
            $kandang = new Kandang;
            $kandang->user_id = $user->id;
            $kandang->save();
        }
        
        $user->save();

        return redirect('adminUtama')->with('success', 'Berhasil Membuat User');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)    
    {
        $user = User::findOrFail($id);
        return view('admin.edit', compact('user'));
    }

    
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'username' => 'required|min:5|max:12',
            'name' => 'required|min:5|max:12',
            'group' => 'required|string',
            'alamat' => 'required|min:5',
            'kota' => 'required',
            'telepon' => 'required|string',
            'stok' => 'required|string',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user->update($request->all());

        return redirect('adminUtama')->with('success', 'Berhasil Mengedit User');
    }

    
    public function destroy(User $user, $id)
    {
        DB::beginTransaction();
        try{
            $user = User::findOrFail($id);
            $peternak = Kandang::where('user_id','=',$id)->first();
            if($peternak->gambar)
            {
                $old_img = $peternak->gambar;
                $filepath = public_path().DIRECTORY_SEPARATOR.'image/ayam'.DIRECTORY_SEPARATOR.$peternak->gambar;
                try{
                    File::delete($filepath);
                }catch(FileNotFoundException $e){
                    //file sudah dihapus
                }
            }
            $user->kandang()->delete();
            $user->delete();

        }catch(Exception $e){
            DB::rollBack();
            Session::flash('error', $e->getMessage())->error();
            return View::make('flash');
        }
        DB::commit();

        return redirect('adminUtama')->with('error', 'User telah dihapus');
    }

    protected function validateUser()
    {
        return request()->validate([
            'username' => 'required|min:5|max:12|unique:users',
            'name' => 'required|min:5|max:12',
            'group' => 'required|string',
            'alamat' => 'required|min:5',
            'kota' => 'required',
            'telepon' => 'required|string',
            'stok' => 'required|string',
            'harga' => 'required|string',
            'email' => 'required',
            'password' => 'required',
        ]);
    }
}
