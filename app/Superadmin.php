<?php

namespace Peternakan;

use Illuminate\Database\Eloquent\Model;

class Superadmin extends Model
{
    protected $fillable = [
        'username', 'name', 'email', 'password', 'group', 'alamat', 'kota', 'telepon'
    ];
}
