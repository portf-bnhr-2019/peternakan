<?php

namespace Peternakan;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $fillable = [
        'peternak_id', 'pembeli_id', 'nama', 'alamat', 'jumlah'
    ];

    public function kandangs()
    {
        return $this->belongsTo('Peternakan\Kandang', 'peternak_id');
    }

    public function pembelis()
    {
        return $this->belongsTo('Peternakan\Pembeli', 'pembeli_id');
    }
}
