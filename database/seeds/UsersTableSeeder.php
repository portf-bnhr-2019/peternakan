<?php

use Illuminate\Database\Seeder;
use Peternakan\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->truncate();

        $user = User::create( [
            'id'=>1,
            'username'=>'superadmin',
            'name'=>'superadmin',
            'group'=>1,
            'alamat'=>'Semarang',
            'kota'=>'Semarang',
            'telepon'=>'62876765563',
            'email'=>'admin@super.com',
            'password'=>'$2y$10$t9Ig2pUygfkGzMK1OQt5nOcG2udJkLGxMi.N75pqYRvLYkAu/h.xq', // superadmin1
            'remember_token'=>NULL,
            'created_at'=>'2019-28-06 06:35:28',
            'updated_at'=>'2019-28-06 06:35:28'
            ]);
    }
}