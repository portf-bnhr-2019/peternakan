<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('peternak_id');
            $table->unsignedInteger('pembeli_id');
            $table->string('nama');
            $table->text('alamat');
            $table->unsignedInteger('jumlah');
            $table->timestamps();

            $table->foreign('peternak_id')->references('id')->on('kandangs')->onDelete('cascade');
            $table->foreign('pembeli_id')->references('id')->on('pembelis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
