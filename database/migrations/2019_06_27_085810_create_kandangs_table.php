<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKandangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kandangs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->date('tanggal')->nullable();
            $table->dateTime('waktu')->nullable();
            $table->char('suhu_1', 100)->nullable();
            $table->char('kelembapan_1', 100)->nullable();
            $table->char('suhu_2', 100)->nullable();
            $table->char('kelembapan_2', 100)->nullable();
            $table->char('suhu_3', 100)->nullable();
            $table->char('kelembapan_3', 100)->nullable();
            $table->unsignedInteger('stok')->nullable();
            $table->integer('harga')->nullable();
            $table->string('gambar')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kandangs');
    }
}
